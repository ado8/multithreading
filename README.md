

# Einführung in die Multi-Thread-Programmierung


[TOC]


Die Multi-Thread-Programmierung zu starten ist einfach, aber schwer, sie wirklich zu beherrschen. Wir werden uns auf den einfachen Anfang konzentrieren und auf einige Herausforderungen hinweisen, wenn es um kompliziertere Programmierung geht.


# Einen neuen Thread erstellen und starten

Die erste Frage ist, wie kann ich einen neuen Thread erstellen und starten. In Java geschieht dies normalerweise durch die Implementierung des Interfaces java.lang.Runnable, die eine Methode run() ohne Argumente und Rückgabewerte enthalten muss. Das bedeutet, dass wir eine Klasse erstellen müssen, die die Runnable-Interface implementiert:

 
	public class MyThread implements Runnable {
	 	public void run(){
	      	// Do what ever the thread should do
	 	}
	}

 

Nun wollen wir einen Thread erstellen. In Java gibt es das Objekt Thread, das diese Aufgabe für uns übernimmt:

 
	Thread myThread1=new Thread(new MyThread());

 

Es ist auch möglich, die Runnable-Interface direkt im Aufruf von new Thread() zu implementieren:

 
	Thread myThread1=new Thread(new Runnable() {
	      @Override
	      public void run() {
	          int i=0;
	          while(true){
	              try {
	                  Thread.sleep(1000);
	              } catch (InterruptedException e) {
	                  e.printStackTrace();
	              }
	              System.out.println("Message of thread 1 second "+i);
	              i++;
	          }
	  	}
	});

 

Das war's, wir haben einen neuen Thread myThread1 erstellt. Aber wie starten wir diesen Thread? Rufen Sie einfach die Methode start für das Objekt myThread1 auf:

 
	myThread1.start();

 

Dieser Aufruf kehrt sofort zurück, wenn der Thread erfolgreich gestartet wurde und führt die run()-Methode von MyThread aus.

Der Thread wird beendet, sobald die run()-Methode endet.

 

 


# Verfolgen, ob der Thread beendet wurde

 

Aber wie kann ich wissen, ob der Thread seine Arbeit beendet hat?

Es gibt eine Methode join() für das Objekt myThread1, die die Ausführung zusammenfasst und zurückgibt, wenn der Thread beendet ist.

 
	myThread1.join();

 

Wann sollte ich join() aufrufen?



* Die häufigste Situation ist, wenn Sie warten wollen, bevor Sie die main()-Methode verlassen, in der Sie alle Threads gestartet haben.
* Wenn Sie warten wollen, bis ein bestimmter Thread seine Arbeit beendet hat. 

 

 

 


# Anhalten eines Threads oder aller Threads

 

Wenn Sie ein Java-Programm mit Threads haben, die in einer Schleife laufen, bis Sie sie beenden wollen, müssen Sie eine gute Möglichkeit haben, die Threads anzuhalten:

 



* Wenn Sie alle Threads am Ende Ihrer main()-Methode stoppen wollen, besteht eine einfache Möglichkeit darin, System.exit(0) als letzten Aufruf zu verwenden. Dadurch werden alle laufenden Threads sofort beendet, mit einem Hard-Kill für sie. -> Dies ist keine saubere Beendigung Ihrer Threads, denn dadurch werden die Threads mitten in der Verarbeitung angehalten.
* Wenn Sie einen bestimmten Thread anhalten wollen, können Sie einfach die stop()-Methode für den Thread aufrufen, z.B. myThread1.stop(). -> Dies ist auch kein sauberes Herunterfahren dieses Threads, da der Thread mitten in der Verarbeitung beendet wird.
* Eine Flag-Variable im myThread1-Objekt setzen und den Thread entscheiden lassen, wann er seine Arbeit beenden soll. -> Dies ist der saubere Weg, wie er in der [API](https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html#stop()) dokumentiert ist.

 

 

Ich habe drei Code-Beispiele erstellt. Sie sind in Gitlab [https://gitlab.com/ado8/multithreading](https://gitlab.com/ado8/multithreading) im Paket com.doerzbach.app zu finden und heißen:

 

Simple_Thread_with_&lt;name>.java

 

Die drei Beispiele tun dasselbe, aber das Anhalten der Threads wird auf drei verschiedene Arten implementiert, wie oben beschrieben.

 


# Kommunikation zwischen Threads

Kommunikation zwischen Threads

Der Beispielcode zur Demonstration der Kommunikation der Threads befindet sich in den Klassen com.doerzbach.app.Consumer_Producer und den beiden Thread-Klassen com.doerzbach.threads.Consumer und com.doerzbach.threads.Producer im gleichen gitlab Repo https://gitlab.com/ado8/multithreading.

 

Da die Threads unabhängig voneinander laufen, können wir nicht einfach eine Methode des anderen Threads aufrufen, um etwas im Kontext des anderen Threads auszuführen.

Wenn wir eine Methode eines Objekts in einem Thread und eine andere Methode desselben Objekts in einem anderen Thread ausführen, können wir zwei Threads parallel auf einem Objekt laufen lassen. Wir können die Werte von Mitgliedsvariablen durch einen Thread ändern und den geänderten Wert durch den anderen Thread lesen. Auf diese Weise können wir zwischen zwei Threads kommunizieren.

Aber das kann auch Probleme verursachen: Wenn wir beispielsweise von zwei Threads aus gleichzeitig ein Objekt zu einer LinkedList hinzufügen möchten, können wir die LinkedList manipulieren, denn das Hinzufügen eines Objekts zur LinkedList erfordert mehrere Operationen, und erst wenn alle abgeschlossen sind, sollten wir versuchen, ein weiteres Objekt zur selben LinkedList hinzuzufügen. Das heißt, wir müssen sicherstellen, dass immer nur ein Thread versucht, die LinkedList zu verändern. Die anderen Threads müssen warten.

Dies kann durch die Synchronisierung von Threads in bestimmten Codeabschnitten mit dem Schlüsselwort "synchronized" erreicht werden:

	synchronized (this) {
		// Here comes the code which can only be run
		// by one thread at the time.
		// If there are two sections with this same object
		// in the parenteses here “this”. Only one thread
		// can by in all these sections.
	}

 

Im Beispiel Consumer_Producer in den Beispielen haben wir ein producer_thread Objekt, das von 5 Threads verändert wird, einem producer_thread, der neue Nachrichten erstellt, und 4 consumer_threadsX, die die Nachrichten lesen und ausdrucken. Sie alle ändern die LinkedList-Nachrichten, die Teil des producer_thread-Objekts ist. Der gesamte Abschnitt für die Synchronisierung befindet sich in der Klasse Producer in den beiden Methoden getMessage() und run().

 

Dann gibt es noch ein kleines Detail zu beachten. Was passiert, wenn der consumer_threadX versucht, eine neue Nachricht zu lesen, bevor der producer_thread eine Nachricht zur LinkedList hinzugefügt hat? Dann ist die LinkedList leer und der consumer_threadX muss warten, bis der producer_thread ein Element zur LinkedList hinzufügt. Deshalb wird in der getMessages()-Methode der Producer-Klasse eine wait()-Methode der Thread-Klasse aufgerufen, die bewirkt, dass die Sperre in der Sektion freigegeben und der aktuelle Thread in den Ruhezustand versetzt wird:

	synchronized (this) {
	    while (messages.size() == 0 && isRunning()) {
	        wait();
	} 
	..
	..

Aber wie wird der Thread wieder aufgeweckt? Dies geschieht in der run()-Methode mit Hilfe der notify()-Methode der Thread-Klasse. Dies veranlasst den ersten Thread, der auf dieser Sperre eingeschlafen ist, aufzuwachen und weiter zu arbeiten. Wenn kein Thread wartet, wird der Aufruf zum Aufwachen einfach ignoriert.

	synchronized (this) {
	            messages.add("Message " + i);
	            i++;
	            notify();
	}

Warum gibt es eine notifyAll()-Methode, die am Ende der run()-Methode von producer aufgerufen wird?

	synchronized (this) {
	      running = false;
	      // This notifies all threads waiting for the state change.
	      notifyAll();
	}

Dies sollte alle Threads aufwecken, die auf neue Nachrichten warten, nachdem die letzte Nachricht der LinkedList hinzugefügt wurde und nachdem das Running-Flag auf false gesetzt wurde. Dadurch werden alle consumer_threadsX veranlasst, eine weitere Schleife zu durchlaufen, zu prüfen, ob eine weitere Nachricht vorliegt, und wenn nicht, zu prüfen, ob der producer_thread noch aktiv ist. -> Dies ermöglicht es ihnen, die Ausführung zu beenden, wenn die letzte Nachricht verarbeitet worden ist.
