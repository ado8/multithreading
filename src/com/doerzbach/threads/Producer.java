package com.doerzbach.threads;

import java.util.LinkedList;

/**
 * This class is implementing a simple queue of messages using the LinkedList.
 * It is also implementing the thread populating the queue.
 * The queue and the populating thread are combined here, but they could also be separated in two classes
 * one implementing the queue with all locking and queue handling stuff, and one just running a thread which is
 * populating the queue with new messages.
 */
public class Producer implements Runnable {
    LinkedList<String> messages = new LinkedList<>();
    boolean running = true;

    /**
     * Implementation of run() of the Runnable interface.
     * This is what is run, when the thread is started.
     */
    @Override
    public void run() {
        int i = 0;
        while (i < 100) {
            // This locks the object, in order to avoid, that another thread is modifying the messages list
            synchronized (this) {
                messages.add("Message " + i);
                i++;
                // After modifying the messages list, it notifies one of the threads waiting for the state change,
                // if there is any thread waiting...
                notify();
            }
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        // This is the end of the messages and we would like to stop the process, by setting the running flag to false.
        synchronized (this) {
            running = false;
            // This notifies all threads waiting for the state change.
            notifyAll();
        }
    }

    /**
     * This returns of the producer is still running or has some data left to share
     * @return true if there is still data left to share or the thread is still running
     */
    public boolean isRunning() {
        return messages.size()!=0 || running;
    }

    /**
     * This returns to first message in the list, which means this works in FIFO mode like a queue
     * @return first message in the queue
     * @throws InterruptedException
     */
    public String getMessage() throws InterruptedException {
        // This locks the object, in order to avoid, that another thread is modifying the messages list
        synchronized (this) {
            // Wait until there is a massage in the queue and the thread is still running
            while (messages.size() == 0 && isRunning()) {
                // The wait call releases the lock on this and waits to get notified of a state change by any other
                // thread.
                // This makes it possible to other threads to enter the synchronized(this) section in the object,
                // as for example in the run() method, and change the state and notify this thread.
                wait();
            }
            // Here we just get the first entry of the LinkedList, if there is any
            // other ways this returns null
            return messages.pollFirst();
        }
    }
}
