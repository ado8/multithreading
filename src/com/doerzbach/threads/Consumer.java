package com.doerzbach.threads;

public class Consumer implements Runnable {
    String title;
    Producer producer;

    public Consumer(String title, Producer producer) {
        this.title = title;
        this.producer = producer;
    }

    @Override
    public void run() {
        try {
            String message;
            while (producer.isRunning()) {
                message = producer.getMessage();
                if(message!=null) System.out.println(title + " " + message);
                // This sleep should simulate the time which it takes to do the work with the message
                // got from the producer
                Thread.sleep(1000+(int) (Math.random()*2000));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
