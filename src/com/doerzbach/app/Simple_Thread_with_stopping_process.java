package com.doerzbach.app;

/**
 * This Class should show how to start two independent threads and run them for 20 seconds before the whole process is
 * terminated.
 */
public class Simple_Thread_with_stopping_process {
    public static void main(String[] args) throws InterruptedException {
        //Create a thread 1 which is displaying a message every second
        //by creating a new Interface implementation of Runnable just inline the code
        Thread myThread1=new Thread(new Runnable() {
            // The actual code to run as a thread comes here as an implementation of
            // the run() method defined in the Runnable interface
            @Override
            public void run() {
                int i=0;
                while(true){
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Message of thread 1 second "+i);
                    i++;
                }
            }
        });
        // Create a thread 2 which is displaying a message every 1.5 seconds
        Thread myThread2=new Thread(new Runnable() {
            @Override
            public void run() {
                int i=0;
                while(true){
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Message of thread 2 second "+i*1.5);
                    i++;
                }
            }
        });
        // Start the threads which will eventually invoke run() of the Runnable interface just created above
        myThread1.start();
        myThread2.start();
        // Wait for 20 seconds
        Thread.sleep(20000);
        // Exit the process an all threads in a not so friendly manner
        //
        System.exit(0);
    }
}
