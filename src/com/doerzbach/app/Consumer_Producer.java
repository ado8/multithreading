package com.doerzbach.app;

import com.doerzbach.threads.Consumer;
import com.doerzbach.threads.Producer;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * This class should show how multiple threads can access the same data in a consistent manner.
 * This involves locking of objects in order to avoid multiple access at the same time.
 */
public class Consumer_Producer {
    public static void main(String[] args) throws InterruptedException {
        // This creates a object producer of the Producer class which is implementing the Runnable Interface
        // The producer object is generating massages and stores them in a LinkedList
        // for retrieval by the Consumer threads
        Producer producer=new Producer();
        // Create a thread of this Runnable Interface
        Thread producer_thread=new Thread(producer);
        // Now create the Consumer threads which have a reference to the producer object.
        Thread consumer_thread1=new Thread(new Consumer("Consumer Thread 1:",producer));
        Thread consumer_thread2=new Thread(new Consumer("Consumer Thread 2:",producer));
        Thread consumer_thread3=new Thread(new Consumer("Consumer Thread 3:",producer));
        Thread consumer_thread4=new Thread(new Consumer("Consumer Thread 4:",producer));
        // Start the producer thread first
        producer_thread.start();
        // now start all consumer threads
        consumer_thread1.start();
        consumer_thread2.start();
        consumer_thread3.start();
        consumer_thread4.start();
        // Here we could end the main method but the process would still run the 5 other methods
        // But we can also join a thread and wait for it....
        System.out.println("Wait for producer_thread to end");
        producer_thread.join();
        System.out.println("Now producer_thread has ended");
        System.out.println("Wait for consumer_thread1 to end");
        consumer_thread1.join();
        System.out.println("consumer_thread1 ended");

    }

}
