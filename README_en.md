# Introduction into Multithreaded Programming


[TOC]


Multithreaded Programming is simple to start with, but hard to really master it. We will focus on the simple start and point to some challenges when it comes to more complicated programming.


# Creating a new thread and start it

The first question is, how can I create a new thread and start it. In Java this is normally done by implementing the java.lang.Runnable interface which must contain one method run() with no arguments and return values. This means, that we have to create a class which implements the Runnable interface:
 
	public class MyThread implements Runnable {
	 	public void run(){
	      	// Do what ever the thread should do
	 	}
	}

Now let’s create a thread. In java there is the object Thread which is doing that for us:

	Thread myThread1=new Thread(new MyThread());

It is also possible to implement the Runnable interface directly in the new Thread() call:
 
	Thread myThread1=new Thread(new Runnable() {
	      @Override
	      public void run() {
	          int i=0;
	          while(true){
	              try {
	                  Thread.sleep(1000);
	              } catch (InterruptedException e) {
	                  e.printStackTrace();
	              }
	              System.out.println("Message of thread 1 second "+i);
	              i++;
	          }
	  	}
	});


This is it, we have created a new Thread myThread1. But how do we start this thread? Simply call the method start on the myThread1 object:

	myThread1.start();

This call will immediately return, when the thread successfully starts and runs the run() method of MyThread.

The Thread ends as soon as the run() method ends.

 

 


# Track if the thread has ended

 

But how can I know, if the thread has ended its work?

There is a method join() on the Object myThread1, which will join the execution and return if when the thread exits.

	myThread1.join();

When should I call join()?



* The most common situation is, when you want to wait before you exit the main() method where you started all the threads.
* If you want to wait, till one specific thread has finished its work.

 

 

 

 


# Stopping a thread or all threads

 

If you have a Java program with threads running a loop until you want to finish them, you have to have a good way to stop threads:

 



* If you want to stop all threads at the end of your main() method, a simple way is to call System.exit(0) as the last call. This will end immediately all running threads, with a hard kill on them. -> This is not a clean shutdown of your threads, because this  really stops the threads in the middle of processing.
* If you want to stop one particular thread, you can simply call the stop() method on the thread, e.g. myThread1.stop(). -> This is neither a clean shutdown of this thread, as the thread is killed in the middle of processing.
* Setting a flag variable in the myThread1 object and let the thread decide when it should exit its work. -> This is the clean way as documented in the [API](https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html#stop()).

 

 

I have made three examples of code. They can be found in Gitlab[ https://gitlab.com/ado8/multithreading](https://gitlab.com/ado8/multithreading) in the package com.doerzbach.app and are called:

 

Simple_Thread_with_&lt;name>.java

 

The three examples do the same, but stopping the threads is implemented in the three different manners as described above.

 


# Communication between threads

The example code to demonstrate the communication of the threads can be found in the Classes com.doerzbach.app.Consumer_Producer and the two thread classes com.doerzbach.threads.Consumer and com.doerzbach.threads.Producer in the same gitlab Repo [https://gitlab.com/ado8/multithreading](https://gitlab.com/ado8/multithreading).

 

As threads are running independent of each other, we cannot just call a method of the other thread to run something in the other thread’s context.

If we run one method of an object in one thread and another method of the same object in another thread, we may run two threads in parallel on one object. We may change member variables values by one thread and read the changed value by the other thread. This is how we can communicate between two threads.

But this can also cause problems: For example, if we would like to add an object to a LinkedList from two threads at the same time, we may tamper the LinkedList, because adding an object to the LinkedList takes multiple operations and only if all of them are finished we should try to add another object to the same LinkedList. This means, we need to be sure only one thread is trying to change the LinkedList at any time. The other threads have to wait.

This can be done by synchronizing threads in certain code sections using the “synchronized” keyword:

	synchronized (this) {
	    // Here comes the code which can only be run
	    // by one thread at the time.
	    // If there are two sections with this same object
	    // in the parenteses here “this”. Only one thread
	    // can by in all these sections.
	}

In the example Consumer_Producer in the examples, we have a producer_thread Object which is modified by 5 threads, one producer_thread which creates new messages, and 4 consumer_threadsX which read the messages and print them out. They are all modifying the LinkedList messages which is part of the producer_thread object. All the section for the synchronization are found in the Producer class in the two methods getMessage() and run().

 

Then there is another small detail to look at. What happens if the consumer_threadX is trying to read a new message before the producer_thread has added one to the LinkedList messages? Then the LinkedList is empty and the consumer_threadX has to wait, till the producer_thread is adding an element to the LinkedList. That is why in the getMessages() method of the producer class a wait() method of the Thread class is called, which causes the lock in the section is released and the current thread is put asleep:

	synchronized (this) {
	    while (messages.size() == 0 && isRunning()) {
	        wait();
	} 
	..
	..

But how the thread is going to wake up again? This is done in the run() method using the notify() method of the thread class. This causes the first thread which was put asleep on this lock to wake up and continue to work. If there is no thread waiting, the wake up call is just ignored.

	synchronized (this) {
	            messages.add("Message " + i);
	            i++;
	            notify();
	}

Why is there a notifyAll() method called at the end of the run() method of producer?

	synchronized (this) {
	      running = false;
	      // This notifies all threads waiting for the state change.
	      notifyAll();
	}

This should wake up all threads which are waiting for new messages after the last message has been added to the LinkedList, and after the running flag is set to false. This will cause all consumer_threadsX to run another loop, check if there is another message, and if there is none check if the producer_thread is still running. -> This will make it possible for them to end the execution when the last messages has been processed.
